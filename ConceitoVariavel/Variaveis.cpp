#include <iostream>
#include <tchar.h>

int main()
{
	// Fun��o que configura o console para o modo portugu�s
	_tsetlocale(LC_ALL, _T("portuguese"));

	int NumVidas = 5;
	int Pontuacao = 1350;
	
	std::cout << "******INICIO DO JOGO******" << std::endl;
	std::cout << "Vidas Jogador: " << NumVidas << std::endl;
	std::cout << "Pontua��o: " << Pontuacao << std::endl;
	std::cout << "Tamanho da vari�vel NumVidas: " << sizeof(NumVidas) << "Bytes" << std::endl;
	std::cout << "Tamanho da vari�vel Pontua��o: " << sizeof(Pontuacao) << "Bytes" << std::endl;
	std::cout << "Endere�o que NumVidas ocupa na mem�ria RAM: " << &NumVidas << std::endl;
	std::cout << "Endere�o que Pontua�ao ocupa na mem�ria RAM: " << &Pontuacao << std::endl;
	std::cout << "************************************" << std::endl;
	
	std::cout << "******DURANTE O JOGO******" << std::endl;
	Pontuacao = Pontuacao + 100; // Pontuacao =+ 100;
	std::cout << "Pontua��o: " << Pontuacao << std::endl;
	NumVidas = NumVidas - 1; // NumVidas =- 1;
	std::cout << "Vidas Jogador: " << NumVidas << std::endl;
	std::cout << "************************************" << std::endl;
	
	system("PAUSE");
	return 0;
}