#include <iostream>
#include <tchar.h>

int main()
{
	_tsetlocale(LC_ALL, _T("portuguese"));
	int Numero01;
	int Numero02;
	int Resultado;
	bool Comparacao;
	std::cout << "Digite o primeiro n�mero: ";
	std::cin >> Numero01;
	std::cout << "Numero01 = " << Numero01 << std::endl;
	std::cout << "Digite o segundo n�mero: ";
	std::cin >> Numero02;
	std::cout << "Numero02 = " << Numero02 << std::endl;
	Resultado = Numero01 + Numero02;
	std::cout << "O Resultado dos dois n�meros �: " << Resultado << std::endl;

	// Operadores relacionais e de igualdade
	Comparacao = (Numero01 == Numero02);
	std::cout << "N�mero01 � igual a N�mero02? " << Comparacao << std::endl;
	Comparacao = (Numero01 != Numero02);
	std::cout << "N�mero01 diferente de N�mero02? " << Comparacao << std::endl;
	Comparacao = (Numero01 > Numero02);
	std::cout << "N�mero01 maior que N�mero02? " << Comparacao << std::endl;
	Comparacao = (Numero01 < Numero02);
	std::cout << "N�mero01 menor que N�mero02? " << Comparacao << std::endl;
	Comparacao = (Numero01 >= Numero02);
	std::cout << "N�mero01 maior ou igual a N�mero02? " << Comparacao << std::endl;
	Comparacao = (Numero01 <= Numero02);
	std::cout << "N�mero01 menor ou igual a N�mero02? " << Comparacao << std::endl;
	
	system("PAUSE");
	return 0;
}