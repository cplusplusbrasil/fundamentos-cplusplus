#include <iostream>
#include <locale.h>
#include "FuncoesMenu.h"

void exibirMenu()
{
	setlocale(LC_ALL, "portuguese");
	do
	{
		std::cout << "\n****Tickets Cinema****" << std::endl;
		std::cout << "\n1 - Para Meia Entrada\n";
		std::cout << "\n2 - Para Inteira\n";
		std::cout << "\n3 - Sair";
	} while (processarEscolha(retornarEscolha()) != 3);
}

int retornarEscolha()
{
	int escolha;
	std::cout << "\nEscolha sua op��o: ";
	std::cin >> escolha;
	return escolha;
}

int processarEscolha(int TipoEscolha)
{
	switch (TipoEscolha)
	{
		case 1:
			std::cout << "\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n";
			std::cout << "\nTicket Meia Entrada Comprada\n";
			std::cout << "\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n";
			break;
		case 2:
			std::cout << "\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n";
			std::cout << "\nTicket Inteira Comprado\n";
			std::cout << "\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n";
			break;
		case 3:
			std::cout << "\nxxxxxxxxxxxxxxxxxxxxxxxx\n";
			std::cout << "\nSaindo Menu Tickets...\n";
			std::cout << "\nxxxxxxxxxxxxxxxxxxxxxxxx\n";
			break;
		default: 
			std::cout << "\nOp��o Inv�lida!!!\n";
			break;
	}
	
	return TipoEscolha;
}