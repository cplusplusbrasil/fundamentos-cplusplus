#include <iostream>

int main()
{
	double MaiorNumero, Numero01, Numero02;
	
	std::cout << "Digite o primeiro numero: ";
	std::cin >> Numero01;
	std::cout << "\nDigite o segundo numero: ";
	std::cin >> Numero02;

	MaiorNumero = (Numero01 > Numero02) ? Numero01 : Numero02;
	std::cout << "\nO maior numero e: " << MaiorNumero << "\n";
	
	(Numero01 > Numero02) ? std::cout << "Numero01 e maior que Numero02\n" << std::endl : std::cout << "Numero02 e maior que Numero01\n" << std::endl;
	
	system("PAUSE");
	return 0;
}