#include <iostream>

int main()
{
	int Numero = 0;
	
	while (Numero <= 50)
	{
		// N�meros �mpares
		if (Numero % 2 != 0)
		{
			std::cout << Numero << std::endl;
		}
		
		// N�meros pares
		if(Numero % 2 == 0)
		{
			std::cout << Numero << std::endl;
		}

		Numero++;
	}
	
	system("PAUSE");
	return 0;
}