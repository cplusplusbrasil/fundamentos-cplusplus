#include <iostream>
#include <tchar.h>

int main()
{
	_tsetlocale(LC_ALL, _T("portuguese"));
	float Valor01;
	float Valor02;
	float Valor03;
	float Media{0.0};
	
	std::cout << "Digite o primeiro valor: ";
	std::cin >> Valor01;
	std::cout << "Digite o segundo valor: ";
	std::cin >> Valor02;
	std::cout << "Digite o terceiro valor: ";
	std::cin >> Valor03;
	std::cout << "N�meros digitados: " << Valor01 << " " << Valor02 << " " << Valor03 << std::endl;
	Media = (Valor01 + Valor02 + Valor03) / 3;
	std::cout << "M�dia dos n�meros: " << Media << std::endl;
	system("PAUSE");
	return 0;
}