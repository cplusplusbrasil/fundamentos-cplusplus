#include <iostream>
#include <tchar.h>
#include <string>

int main()
{
	_tsetlocale(LC_ALL, _T("portuguese"));
	int NumeroInteiro{12};
	float NumeroReal{34.56f};
	double NumeroReal2{23456.56366};
	char Caractere{'c'};
	std::string Texto{"Texto da string"};
	std::printf("Valor de NumeroInteiro: %d\n", NumeroInteiro);
	std::printf("Valor de NumeroReal: %.2f\n", NumeroReal);
	std::printf("Valor de NumeroRealDouble: %lf\n", NumeroReal2);
	std::printf("Valor de Caractere: %c\n", Caractere);
	std::printf("Valor de Texto: %s\n", Texto.c_str());
	std::cout << "Valor da string: " << Texto << std::endl;
	
	std::printf("O valor do caractere digitado: %c na tabela ASCII �: %d\n", Caractere, Caractere);
	
	system("PAUSE");
	return 0;
}