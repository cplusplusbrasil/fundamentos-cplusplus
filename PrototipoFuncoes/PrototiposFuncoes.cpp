#include <iostream>

// Prot�tipo de fun��o (soma)
float soma(float num1, float num2);
bool isPar(int num);

int main()
{
	int num;
	std::cout << "Digite um numero: ";
	std::cin >>	num;
	
	if (isPar(num))
		std::cout << "Numero: " << num << " PAR" << std::endl;
	else
		std::cout << "Numero: " << num << " IMPAR" << std::endl;
	
	//std::cout << soma(100.50f, 500.55f) << std::endl;
	system("PAUSE");
	return 0;
}

float soma(float num1, float num2)
{
	return num1 + num2;
}

bool isPar(int num)
{
	if (num % 2 == 0)
		return true;
	else
		return false;
}