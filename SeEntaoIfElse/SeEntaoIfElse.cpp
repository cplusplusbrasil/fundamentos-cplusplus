#include <iostream>
#include <string>
#include <tchar.h>

int main()
{
	_tsetlocale(LC_ALL, _T("portuguese"));
	bool bFezSol, bCarroPronto, bSalarioDepositado;
	bool bAcesso;
	std::string SenhaDeAcesso = "cmaismais";
	std::string SenhaDigitada;

	std::cout << "Digite a senha de acesso: ";
	std::cin >> SenhaDigitada;
	
	if (SenhaDigitada == SenhaDeAcesso)
	{
		std::cout << "\nAcesso Permitido!" << std::endl;
		bAcesso = true;
		//system("PAUSE");
	}
	else
	{
		std::cout << "\nAcesso Negado!" << std::endl;
		system("PAUSE");
		exit(0);
	}
	
	//bFezSol = bCarroPronto = bSalarioDepositado = true;
	bFezSol = false;
	bCarroPronto = false;
	bSalarioDepositado = true;

	if (bAcesso)
	{
		if(bFezSol && bCarroPronto && bSalarioDepositado)
		{
			std::cout << "\nVai dar praia!" << std::endl;
		}
		else if(bSalarioDepositado)
		{
			std::cout << "\nN�o vai d� praia! Porque o sal�rio foi depositado, mais n�o fez sol e o carro n�o ficou pronto." << std::endl;
		}
	}

	system("PAUSE");
	return 0;
}