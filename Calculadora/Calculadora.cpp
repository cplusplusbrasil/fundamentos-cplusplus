#include <iostream>
#include <tchar.h>

float soma(float Num1, float Num2);
float subtracao(float Num1, float Num2);
float multiplicacao(float Num1, float Num2);
float divisao(float Num1, float Num2);

int main()
{
	_tsetlocale(LC_ALL, _T("portuguese"));
	float Num1, Num2;
	
	std::cout << "Digite um n�mero: ";
	std::cin >> Num1;
	
	std::cout << "Digite outro n�mero: ";
	std::cin >> Num2;
	
	std::cout << "\n****RESULTADO DAS OPERA��ES****" << "\n";
	std::cout << Num1 << " e " << Num2 << "\n";
	std::cout << "Soma: " << soma(Num1, Num2) << "\n";
	std::cout << "Subtra��o: " << subtracao(Num1, Num2) << "\n";
	std::cout << "Multiplica��o: " << multiplicacao(Num1, Num2) << "\n";
	std::cout << "Divis�o: " << divisao(Num1, Num2) << "\n";
	
	system("PAUSE");
	return 0;
}

float soma(float Num1, float Num2)
{
	return Num1 + Num2;
}

float subtracao(float Num1, float Num2)
{
	return Num1 - Num2;
}

float multiplicacao(float Num1, float Num2)
{
	return Num1 * Num2;
}

float divisao(float Num1, float Num2)
{
	return Num1 / Num2;
}