#include <iostream>
#include <iomanip>
#include <fcntl.h>
#include <io.h>

int main()
{
	/*_setmode(_fileno(stdout), _O_U16TEXT);
	std::wcout << L"\u00AE" << L"\u263A" << std::endl;
	system("PAUSE");*/

	bool bAchou;
	bAchou = 1;
	std::cout << "Valor de bAchou: " << bAchou << std::endl;
	bAchou = 0;
	std::cout << "Valor de bAchou: " << bAchou << std::endl;
	bAchou = true;
	std::cout << "Valor de bAchou: " << bAchou << std::endl;
	bAchou = false;
	std::cout << "Valor de bAchou: " << bAchou << std::endl;
	bAchou = 'd';
	std::cout << "Valor de bAchou: " << bAchou << std::endl;
	bAchou = "1002545A";
	std::cout << "Valor de bAchou: " << bAchou << std::endl;
	
	int Numero;
	float Numero2;
	double Numero3;
	char Caractere = '3';
	char Caractere2 = 'b';
	
	Numero = 45;
	Numero2 = 55.56f;
	Numero3 = 45345.904555;
	
	std::cout << "Valor Numero : " << Numero << std::endl;
	std::cout << "Tamanho da Variavel: " << sizeof(Numero) << " Bytes" << std::endl;
	std::cout << "Endereco Carregado na Memoria: " << &Numero << std::endl;

	std::cout << "Valor Numero2 : " << Numero2 << std::endl;
	std::cout << "Tamanho da Variavel: " << sizeof(Numero2) << " Bytes" << std::endl;
	std::cout << "Endereco Carregado na Memoria: " << &Numero2 << std::endl;

	std::cout << "Valor Numero3 : " << std::setprecision(12) << Numero3 << std::endl;
	std::cout << "Tamanho da Variavel: " << sizeof(Numero3) << " Bytes" << std::endl;
	std::cout << "Endereco Carregado na Memoria: " << &Numero3 << std::endl;

	std::cout << "Valor Caractere : " << Caractere << std::endl;
	std::cout << "Tamanho da Variavel: " << sizeof(Caractere) << " Byte" << std::endl;
	std::cout << "Endereco Carregado na Memoria: " << (void *)& Caractere << std::endl;

	std::cout << "Valor Caractere2 : " << Caractere2 << std::endl;
	std::cout << "Tamanho da Variavel: " << sizeof(Caractere2) << " Byte" << std::endl;
	std::cout << "Endereco Carregado na Memoria: " << (void *)& Caractere2 << std::endl;
	
	system("PAUSE");
	return 0;
}