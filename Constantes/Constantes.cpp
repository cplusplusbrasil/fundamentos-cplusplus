#include <iostream>

int main()
{
	const int NUM_VIDAS = 10;
	int TotalDeVidas;
	TotalDeVidas = NUM_VIDAS - 1;
	std::cout << "Total de Vidas: " << TotalDeVidas << std::endl;
	std::cout << "Valor Constante NUM_VIDAS: " << NUM_VIDAS << std::endl;
	std::cout << "Endereco de Memoria de NUM_VIDAS: " << &NUM_VIDAS << std::endl;
	system("PAUSE");
}