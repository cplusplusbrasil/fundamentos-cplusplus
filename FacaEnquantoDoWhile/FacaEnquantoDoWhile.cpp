#include <iostream>
#include <locale>

int main()
{
	setlocale(LC_ALL, "portuguese");
	int Escolha = 0;
	
	do
	{
		std::cout << "*******TICKETS CINEMA*******" << std::endl;
		std::cout << "1 - Meia Entrada" << std::endl;
		std::cout << "2 - Inteira" << std::endl;
		std::cout << "3 - Sair" << std::endl;
		std::cout << "Escolha: ";
		std::cin >> Escolha;
		
		switch (Escolha)
		{
			case 1:
				std::cout << "Ticket Meia Entrada Comprada!" << std::endl;
				break;
			case 2:
				std::cout << "Ticket Inteira Comprada!" << std::endl;
				break;
			case 3:
				std::cout << "Saindo..." << std::endl;
				exit(0);
				break;
		default:
			std::cout << "Op��o inv�lida" << std::endl;
			break;
		}
	} while (Escolha != 3);
	
	system("PAUSE");
	return 0;
}