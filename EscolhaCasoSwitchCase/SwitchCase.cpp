#include <iostream>

int main()
{
	int Num1, Num2;
	char Operacao;
	std::cout << "\nDigite um numero: ";
	std::cin >> Num1;
	std::cout << "\nDigite outro numero: ";
	std::cin >> Num2;
	std::cout << "\nQual operacao deseja fazer?\n";
	std::cout << "0 - Sair\n";
	std::cout << "1 - Adicao\n";
	std::cout << "2 - Subtracao\n";
	std::cout << "3 - Multiplicacao\n";
	std::cout << "4 - Divisao\n";
	std::cin >> Operacao;
	
	switch (Operacao)
	{
		case '0':
			std::cout << "\nSaindo...\n" << std::endl;
			exit(0);
			break;
		case '1':
			std::cout << "\n" << "O resultado e: " << Num1 + Num2 << "\n";
			break;
		case '2':
			std::cout << "\n" << "O resultado e: " << Num1 - Num2 << "\n";
			break;
		case '3':
			std::cout << "\n" << "O resultado e: " << Num1 * Num2 << "\n";
			break;
		case '4':
			std::cout << "\n" << "O resultado e: " << Num1 / Num2 << "\n";
			break;
	default:
		std::cout << "\nOperacao invalida\n";
		break;
	}
	system("PAUSE");
	return 0;
}